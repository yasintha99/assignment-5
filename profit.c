#include<stdio.h>

int income(int price,int attendance);
int costForAttendees(int attendeeCost,int attendance);
int calProfit(int price,int attendeeCost,int attendance);

int income(int price,int attendance)
{
  return price*attendance;
}
int costForAttendees(int attendeeCost,int attendance)
{
  return attendeeCost*attendance;
}
int calProfit(int price,int attendeeCost,int attendance)
{
  return income(price,attendance)-costForAttendees(attendeeCost,attendance)-500;
}
int main(){
  int maxProfit=0;
  int bestTicketPrice=0;
  const int attendeeCost=3;
  int price=0;
  int attendance=160;
  printf("Price of ticket(Rs)        ");
  printf("Profit(Rs)\n");

  while (attendance>0)
  {
    price=price+05;
    printf("  %d",price);
    printf("       =======>        ");
    printf(" %d \n",calProfit(price,attendeeCost,attendance));

    if(calProfit(price,attendeeCost,attendance)> maxProfit)
    {
      maxProfit = calProfit(price,attendeeCost,attendance);
      bestTicketPrice = price;
    }
    attendance= attendance-20;
  }
  printf("\n \n");
  printf("Best ticket price is Rs. %d           Highest profit is Rs. %d \n",bestTicketPrice,maxProfit);
  return 0;
}
